# Signal-Desktop-Themer

a proof-of-concept Bash script to inject CSS into the (flatpak^1) build of Signal Desktop.

1: Currently only the Linux Flatpak due to limited time for testing and developing. Ports to other systems, packages and alike welcome.

## DISCLAIMER:
**Currently there is NO automatic reversal function. You will have to manually do the following**:

```
cd "$(flatpak info -l org.signal.Signal)/files/Signal/resources/app-asar-unpack/stylesheets"
mv manifest.css.backup manifest.css
cd "$(flatpak info -l org.signal.Signal)/files/Signal"
bunx @electron/asar pack resources/app-asar-unpack resources/app.asar 
```
Alternatively, re-install signal (without removing data):
```
flatpak remove signal -y
flatpak install signal -y
```

**Additionally, this was only tested om a user-install of Signal. You will likely need `sudo` if you want to use this with a System-Wide installation.** This is untested, however.

## Dependencies:

Currently this depends on some utilities being installed on your system. Most of these you should already have.

- `sed`
- `bunx` or `npx` (Bun or NodeJS)
- `bash` (script isn't designed to be POSIX-Compliant)

## Getting Started:

You can either head to the [releases section](https://codeberg.org/TGRush/Signal-Desktop-Themer/releases) and download the latest "stable" main.sh there, or clone the repository:
```
git clone --depth 1 https://codeberg.org/TGRush/Signal-Desktop-Themer
``` 

Then, place a `custom.css` in the same folder as the main.sh, and fill it with the CSS you want applied.

run `./main.sh --inject` and follow on-screen prompts.
