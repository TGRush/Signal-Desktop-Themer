#!/bin/bash

set -e

license="
#	 Signal-Desktop-Themer
#    Copyright (C) 2024 - Alexia Müller (cyrus42@disroot.org)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"

usage="
$0 {OPTIONS} {ARGUMENTS}
[] = Mandatory
{} = Optional

--inject	injects CSS of your choice into Signal Desktop.
	NOTE: Place the CSS in $PWD/custom.css
--help		shows this help page
--license 	displays the license notice
"

function _dependencies() {
	if [[ $(which bunx) ]]; then
		echo "bunx found! We're all good!"
		_node_runner="bunx"
	elif [[ $(which npx) ]]; then
		echo "npx found! We're all good!"
		_node_runner="npx"
	else
		echo "Please install NodeJS or Bun! We need the bunx or npx utilities."
		echo "If they're in a nonstandard place, or not in your \$PATH, the script may not work."
		echo "NOTE: If both are installed, bunx will take priority."
	fi
}

function _flatpak() {
	# Set Signal Dir to corresponding Flatpak-Provided Dir automatically
	_signal_dir="$(flatpak info -l org.signal.Signal)/files/Signal/resources"
}

function _main() {
	echo "NOTE: Currently only Flatpak is supported, if this is okay with you, hit ENTER."
	echo "Otherwise, hit CTRL+C."
	echo ""
	read -rp "Press Enter to continue" </dev/tty
	echo ""
	echo "Signal DIR is: ${_signal_dir}"
	echo "Node Runner is: ${_node_runner}" 
	${_node_runner} @electron/asar extract "${_signal_dir}/app.asar" "${_signal_dir}/app-asar-unpack/"
	cp ./custom.css "${_signal_dir}/app-asar-unpack/stylesheets/custom.css"
	cp "${_signal_dir}/app-asar-unpack/stylesheets/manifest.css" "${_signal_dir}/app-asar-unpack/stylesheets/manifest.css.backup"
	sed -i '1s/^/\n/' "${_signal_dir}/app-asar-unpack/stylesheets/manifest.css"
	sed -i '1s/^/@import url("custom.css");/' "${_signal_dir}/app-asar-unpack/stylesheets/manifest.css"
	${_node_runner} @electron/asar pack "${_signal_dir}/app-asar-unpack/" "${_signal_dir}/app.asar"
}

function _help() {
	echo "$usage"
	return
}

function _license() {
	echo "$license"
	return
}

case $@ in
	--help)
		_help;;
	--license)
		_license;;
	--inject)
		_dependencies
		_flatpak
		_main;;
	*)
		echo "no valid arguments!"
		_help;;
esac
