#!/bin/bash


# This was modified from a Discord Thread on the catppuccin discord server. I do not own it and included it here as a little transitional way to get Catppuccin on Signal Desktop.

# I've also modified it to use the Flatpak path.

signal_path="$(flatpak info -l org.signal.Signal)/files/Signal/resources/app.asar"

# Sidebar
sed -i 's/#2e2e2e/#11111b/g' "$signal_path"

# chat background
sed -i 's/#121212/#181825/g' "$signal_path"

# message bubbles
sed -i 's/#3b3b3b/#1e1e2e/g' "$signal_path"

# line between side bar and chat
sed -i 's/#4a4a4a/#313244/g' "$signal_path"

# selected chat background
sed -i 's/#314244/#181825/g' "$signal_path"    

# audio buttons
sed -i 's/#5e5e5e/#45475a/g' "$signal_path"    
sed -i 's/#dedede/#6c7086/g' "$signal_path"    

# text color
sed -i 's/#e9e9e9/#cdd6f4/g' "$signal_path"    
sed -i 's/#b9b9b9/#bac2de/g' "$signal_path" 
sed -i 's/#f6f6f6/#cdd6f4/g' "$signal_path"

# link color
sed -i 's/#2c6bed/#89b4fa/g' "$signal_path"

# unread stories icon
sed -i 's/#f44336/#f38ba8/g' "$signal_path"

# signal icon
sed -i 's/#3a76f0/#89b4fa/g' "$signal_path"

